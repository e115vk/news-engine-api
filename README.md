# News engine api

## How to start

### Locally
- create database
- Fill `.env` with your db creds:
  - DB_HOST
  - DB_PORT
  - DB_USER
  - DB_NAME
  - DB_PASSWORD
  - JWT_SECRET
  - SALT
  - JWT_EXPIRATION
- `yarn`
- `yarn server`

### Using docker-compose

`docker-compose up`
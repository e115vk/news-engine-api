FROM node:12-alpine

WORKDIR /news-engine-api

COPY package.json ./
COPY yarn.lock ./
COPY tsconfig.json ./
COPY .env.compose ./.env

COPY node_modules ./node_modules
COPY app ./app

EXPOSE 3030

CMD ["yarn", "server"]
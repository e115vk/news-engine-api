import * as request from 'supertest';
import { Express } from 'express';
import 'should';

import { NewsEngineAPI } from '@app/service';
import { Commentary, News, UserService } from '@app/entities';
import { getRepository } from 'typeorm';
import { TestHelper } from '@app/common';

const app = new NewsEngineAPI();
let server: Express | undefined;
let newsId: string | undefined;
let token: string | undefined;
let userId: string | undefined;

describe('Commentary controller test', () => {
  before(async () => {
    await app.start();
    await TestHelper.createFakeDatabase();
    server = app.server;
  });

  beforeEach(async () => {
    const someNews = await getRepository(News).findOneOrFail({ relations: ['author'] });
    newsId = someNews.id;
    userId = someNews.author.id;
    token = UserService.generateUserToken(userId);
  });

  after(async () => {
    await TestHelper.removeFakeDatabase();
    await app.stop();
  });

  describe('GET /api/news/:id/commentary', () => {
    it('should get news commentary', async () => {
      const { body: response } = await request(server)
        .get(`/api/news/${newsId!}/commentary`)
        .expect(200);

      response.data.should.instanceOf(Array);
      response.data.should.have.length(10);
      response.data.forEach(validateNewsCommentaryItem);
      response.count.should.be.eql(20);
    });

    it('should get news commentary with params limit', async () => {
      const { body: response } = await request(server)
        .get(`/api/news/${newsId!}/commentary?limit=5`)
        .expect(200);

      response.data.should.instanceOf(Array);
      response.data.should.have.length(5);
      response.data.forEach(validateNewsCommentaryItem);
      response.count.should.be.eql(20);
    });

    it('should get news commentary with params limit and offset', async () => {
      const { body: response1 } = await request(server)
        .get(`/api/news/${newsId!}/commentary?limit=1&offset=1`)
        .expect(200);

      const { body: response2 } = await request(server)
        .get(`/api/news/${newsId!}/commentary?limit=1&offset=2`)
        .expect(200);

      response1.should.not.be.eql(response2);
    });

    it('should return 422 with incorrect params limit and offst', async () => {
      await request(server)
        .get(`/api/news/${newsId!}/commentary??limit=asdasd&offset=pirod`)
        .expect(422);
    });
  });

  describe('POST /api/news/:id/commentary', () => {
    it('should return 401 unauthorized', async () => {
      await request(server)
        .post(`/api/news/${newsId!}/commentary`)
        .send({ text: 'My awesome text' })
        .expect(401);
    });

    it('should create commentary', async () => {
      const { body: response } = await request(server)
        .post(`/api/news/${newsId!}/commentary`)
        .send({ text: 'My awesome text' })
        .set('Authorization', `Bearer ${token}`)
        .expect(201);

      validateNewsCommentaryItem(response);
    });

    [{}, [{}], [], null, undefined].forEach(value => {
      it(`should return 422 text: ${value}`, async () => {
        await request(server)
          .post(`/api/news/${newsId!}/commentary`)
          .send({ text: value })
          .set('Authorization', `Bearer ${token}`)
          .expect(422);
      });
    });

    it('should return 422 for incorrect news id', async () => {
      await request(server)
        .post(`/api/news/${token!}/commentary`)
        .send({ text: 'My awesome text' })
        .set('Authorization', `Bearer ${token}`)
        .expect(422);
    });

    it('should not create commentary for non-existing news', async () => {
      await request(server)
        .post(`/api/news/${userId!}/commentary`)
        .send({ text: 'My awesome text' })
        .set('Authorization', `Bearer ${token}`)
        .expect(404);
    });
  });
});

function validateNewsCommentaryItem(item: Commentary) {
  ['createdAt', 'text', 'id'].forEach(key => {
    item[key as keyof Commentary].should.not.be.undefined();
    item[key as keyof Commentary].should.be.instanceOf(String);
  });

  const { name, email, id } = item.author;
  name.should.not.be.undefined();
  email.should.not.be.undefined();
  id.should.not.be.undefined();
  name.should.be.instanceOf(String);
  email.should.be.instanceOf(String);
  id.should.be.instanceOf(String);
}

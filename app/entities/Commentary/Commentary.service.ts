import { getRepository } from 'typeorm';
import { Commentary, User } from '@app/entities';
import { validate } from 'class-validator';
import { NewsService } from '@entities/News/News.service';
import { ValidationError } from '@app/common';

export class CommentaryService {
  public static async getListByNewsId(id: string, limit = 10, offset = 0) {
    if (isNaN(limit)) {
      throw new ValidationError('Limit should be number', '.limit');
    }
    if (isNaN(offset)) {
      throw new ValidationError('Offset should be number', '.offset');
    }

    const [commentaries, count] = await getRepository(Commentary).findAndCount({
      where: { news: { id } },
      relations: ['author'],
      take: limit,
      skip: offset,
      order: {
        createdAt: 'ASC'
      }
    });

    return { data: commentaries.map(item => item.formatToResponse()), count };
  }

  public static async create(text: string, newsId: string, author: User) {
    const commentaryInstance = new Commentary(text);

    const errors = await validate(commentaryInstance);

    if (errors.length) {
      const firstError = errors[0];
      throw new ValidationError(JSON.stringify(firstError.constraints), `.${firstError.property}`);
    }
    const newsInstance = await NewsService.getByIdOrFail(newsId);

    const result = await getRepository(Commentary).save({ ...commentaryInstance, news: newsInstance, author });

    return this.getById(result.id);
  }

  public static async getById(id: string) {
    const result = await getRepository(Commentary).findOneOrFail(id, { relations: ['author'] });
    return result.formatToResponse();
  }
}

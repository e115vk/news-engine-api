import { Column, Entity, JoinColumn, ManyToOne, PrimaryGeneratedColumn } from 'typeorm';
import { Length } from 'class-validator';

import { News } from '@app/entities/News/News.model';
import { User } from '@entities/User/User.model';

@Entity()
export class Commentary {
  constructor(text: string) {
    this.text = text;
  }

  @PrimaryGeneratedColumn('uuid')
  public id: string;

  @ManyToOne(() => News, news => news.commentaries)
  public news: News;

  @Column({
    type: 'timestamp',
    default: () => 'CURRENT_TIMESTAMP'
  })
  public createdAt: Date;

  @Column({ nullable: false })
  @Length(1, 100)
  public text: string;

  @ManyToOne(() => User)
  @JoinColumn()
  public author: User;

  public formatToResponse() {
    const { createdAt, author, text, id } = this;
    return { createdAt, author: author.formatToResponse(), text, id };
  }
}

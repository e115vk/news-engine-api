import * as express from 'express';

import { authenticateMiddleware, HttpError, ValidationError } from '@app/common';
import { CommentaryService } from '@app/entities';

const CommentaryController = express.Router();

CommentaryController.route('/news/:id/commentary')
  .get(async (req, res, next) => {
    try {
      const {
        query: { limit, offset },
        params: { id }
      } = req;

      const result = await CommentaryService.getListByNewsId(id, limit, offset);

      return res.json(result);
    } catch (err) {
      return next(err);
    }
  })
  .post(authenticateMiddleware, async (req, res, next) => {
    try {
      const {
        params: { id },
        body: { text }
      } = req;

      const user = (req as any).user;
      const result = await CommentaryService.create(text, id, user);

      return res.status(201).send(result);
    } catch (err) {
      if (err.code === '22P02') {
        return next(new ValidationError('Invalid id', '.id'));
      }
      if (err.name === 'EntityNotFound') {
        return next(new HttpError(404, 'News not found'));
      }
      console.log(err);
      return next(err);
    }
  });

export { CommentaryController };

import * as request from 'supertest';
import { Express } from 'express';
import 'should';

import { NewsEngineAPI } from '@app/service';
import { News, User, UserService } from '@app/entities';
import { getRepository, Not } from 'typeorm';
import { TestHelper } from '@app/common';

const app = new NewsEngineAPI();
let server: Express | undefined;
let token: string | undefined;
let user: User | undefined;
let news: News | undefined;

describe('News controller test', () => {
  before(async () => {
    await app.start();
    server = app.server;
  });

  beforeEach(async () => {
    await TestHelper.createFakeDatabase();
    news = await getRepository(News).findOneOrFail({ relations: ['author'] });
    user = news.author;

    token = UserService.generateUserToken(user.id);
  });

  afterEach(async () => {
    await TestHelper.removeFakeDatabase();
  });

  after(async () => {
    await TestHelper.removeFakeDatabase();
    await app.stop();
  });

  const createNewsKeys = ['title', 'body'];

  describe('GET /api/news', () => {
    it('should get news', async () => {
      const { body: response } = await request(server)
        .get('/api/news')
        .expect(200);

      response.data.should.instanceOf(Array);
      response.data.should.have.length(10);
      response.data.forEach(validateNewsItem);
      response.count.should.be.eql(20);
    });

    it('should get news with params limit', async () => {
      const { body: response } = await request(server)
        .get('/api/news?limit=2')
        .expect(200);

      response.data.should.instanceOf(Array);
      response.data.should.have.length(2);
      response.data.forEach(validateNewsItem);
      response.count.should.be.eql(20);
    });

    it('should get news with params limit and offset', async () => {
      const { body: response1 } = await request(server)
        .get('/api/news?limit=1&offset=1')
        .expect(200);

      const { body: response2 } = await request(server)
        .get('/api/news?limit=1&offset=2')
        .expect(200);

      response1.should.not.be.eql(response2);
    });

    it('should return 422 with incorrect params limit and offst', async () => {
      await request(server)
        .get('/api/news?limit=asdasd&offset=pirod')
        .expect(422);
    });
  });

  describe('GET /api/news/:id', () => {
    it('should get news by id', async () => {
      const someNews = await getRepository(News).findOneOrFail({ relations: ['author'] });

      const { body: response } = await request(server)
        .get(`/api/news/${someNews.id}`)
        .expect(200);

      validateNewsItem(response);
      response.author.should.be.eql(someNews.author.formatToResponse());

      ['id', 'title', 'body'].forEach(key => {
        response[key].should.be.eql(someNews[key as keyof News]);
      });

      new Date(response.createdAt).should.be.eql(new Date(someNews.createdAt));
    });

    it('should return 422', async () => {
      await request(server)
        .get(`/api/news/incorrectid`)
        .expect(422);
    });

    it('should return 404 not found', async () => {
      const someUser = await getRepository(User).findOneOrFail({});
      await request(server)
        .get(`/api/news/${someUser.id}`)
        .expect(404);
    });
  });

  describe('POST /api/news', () => {
    const createNewsBody = { title: 'My awesome news title', body: 'My awesome news body' };

    it('should create news', async () => {
      const { body: response } = await request(server)
        .post('/api/news')
        .send(createNewsBody)
        .set('Authorization', `Bearer ${token}`)
        .expect(200);

      validateNewsItem(response);
      response.author.should.be.eql(user!.formatToResponse());

      createNewsKeys.forEach(key => {
        response[key].should.be.eql(createNewsBody[key as keyof typeof createNewsBody]);
      });
    });

    [[], {}, 12, [{}, {}], false, undefined].forEach(incorrectValue => {
      createNewsKeys.forEach(incorrectKey => {
        it(`should return 422 ${incorrectKey}: ${incorrectValue}`, async () => {
          await request(server)
            .post('/api/news')
            .send({ ...createNewsBody, [incorrectKey]: incorrectValue })
            .set('Authorization', `Bearer ${token}`)
            .expect(422);
        });
      });
    });

    it('should return 401 unauthorized', async () => {
      await request(server)
        .post('/api/news')
        .send({ ...createNewsBody })
        .expect(401);
    });
  });

  describe('PUT /api/news/:id', () => {
    const body = { title: 'My new awesome title', body: 'My new awesome body' };

    it('should return 401 unauthorized', async () => {
      await request(server)
        .put(`/api/news/${news!.id}`)
        .send(body)
        .expect(401);
    });

    it('should update news by id', async () => {
      const { body: response } = await request(server)
        .put(`/api/news/${news!.id}`)
        .send(body)
        .set('Authorization', `Bearer ${token}`)
        .expect(200);

      response.id.should.be.eql(news!.id);
      response.author.should.be.eql(news!.author.formatToResponse());
      response.title.should.not.be.eql(news!.title);
      response.body.should.not.be.eql(news!.body);
    });

    it('should return 403', async () => {
      const incorrectUser = await getRepository(User).findOneOrFail({ where: { id: Not(user!.id) } });
      const incorrectToken = UserService.generateUserToken(incorrectUser.id);

      await request(server)
        .put(`/api/news/${news!.id}`)
        .send(body)
        .set('Authorization', `Bearer ${incorrectToken}`)
        .expect(403);
    });

    [[], {}, 12, [{}, {}], false, null].forEach(incorrectValue => {
      createNewsKeys.forEach(incorrectKey => {
        it(`should return 422 ${incorrectKey}: ${incorrectValue}`, async () => {
          await request(server)
            .put(`/api/news/${news!.id}`)
            .send({ ...body, [incorrectKey]: incorrectValue })
            .set('Authorization', `Bearer ${token}`)
            .expect(422);
        });
      });
    });
  });
});

function validateNewsItem(item: News) {
  ['createdAt', 'title', 'id'].forEach(key => {
    item[key as keyof News].should.not.be.undefined();
    item[key as keyof News].should.be.instanceOf(String);
  });
}

import { Column, Entity, JoinColumn, ManyToOne, OneToMany, PrimaryGeneratedColumn } from 'typeorm';
import { Length } from 'class-validator';

import { MAX_BODY_LENGTH, MAX_TITLE_LENGTH } from '@entities/News/News.const';
import { Commentary } from '@entities/Commentary/Commentary.model';
import { User } from '@app/entities';

@Entity()
export class News {
  constructor(title: string, body: string) {
    this.title = title;
    this.body = body;
  }

  @PrimaryGeneratedColumn('uuid')
  public id: string;

  @Column({ length: MAX_TITLE_LENGTH, nullable: false })
  @Length(10, MAX_TITLE_LENGTH)
  public title: string;

  @Length(10, MAX_TITLE_LENGTH)
  @Column({ length: MAX_BODY_LENGTH, nullable: false })
  public body: string;

  @Column({
    type: 'timestamp',
    default: () => 'CURRENT_TIMESTAMP'
  })
  public createdAt: Date;

  @ManyToOne(() => User)
  @JoinColumn()
  public author: User;

  @OneToMany(() => Commentary, commentary => commentary.news)
  public commentaries: Commentary[];

  public formatToShortResponse() {
    const { createdAt, title, id } = this;
    return { createdAt: createdAt.toISOString(), title, id };
  }

  public formatToResponse() {
    const { createdAt, author, ...rest } = this;

    return {
      createdAt: createdAt.toISOString(),
      author: author.formatToResponse(),
      ...rest
    };
  }
}

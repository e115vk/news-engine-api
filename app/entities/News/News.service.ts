import { getRepository } from 'typeorm';
import { validate } from 'class-validator';

import { News, User } from '@app/entities';
import { HttpError, ValidationError } from '@common/error/httpError';

export class NewsService {
  public static async getList(limit?: number, offset?: number) {
    if (limit && isNaN(limit)) {
      throw new ValidationError('Limit should be number', '.limit');
    }
    if (offset && isNaN(offset)) {
      throw new ValidationError('Offset should be number', '.offset');
    }

    const [news, count] = await getRepository(News).findAndCount({
      skip: offset || 0,
      take: limit || 10,
      order: { createdAt: 'DESC' }
    });

    return { data: news.map(item => item.formatToShortResponse()), count };
  }

  public static async getById(id: string) {
    const news = await getRepository(News).findOne({
      where: { id },
      relations: ['author']
    });

    if (!news) {
      throw new HttpError(404, 'News not found');
    }
    return news.formatToResponse();
  }

  public static async updateById(id: string, updateData: Partial<News>, userId: string) {
    const newsInstance = await getRepository(News).findOneOrFail({ where: { id }, relations: ['author'] });

    const authorId = newsInstance.author.id;
    if (authorId !== userId) {
      throw new HttpError(403, 'You cannot update this news');
    }

    const updateNews = getRepository(News).merge(newsInstance, updateData);
    const errors = await validate(updateNews);

    if (errors.length) {
      const firstError = errors[0];
      throw new ValidationError(JSON.stringify(firstError.constraints), `.${firstError.property}`);
    }

    await getRepository(News).update(id, updateData);
    return NewsService.getById(id);
  }

  public static async create(author: User, title: string, body: string) {
    const newsInstance = new News(title, body);

    const errors = await validate(newsInstance);

    if (errors.length) {
      const firstError = errors[0];
      throw new ValidationError(JSON.stringify(firstError.constraints), `.${firstError.property}`);
    }

    const { id } = await getRepository(News).save({ author, ...newsInstance });

    return this.getById(id);
  }

  public static getByIdOrFail(id: string) {
    return getRepository(News).findOneOrFail(id);
  }
}

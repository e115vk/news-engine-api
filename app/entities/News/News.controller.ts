import * as express from 'express';

import { NewsService } from '@app/entities';
import { authenticateMiddleware, ValidationError } from '@app/common';

const NewsController = express.Router();

NewsController.route('/news')
  .get(async (req, res, next) => {
    try {
      const { limit, offset } = req.query;
      return res.json(await NewsService.getList(limit, offset));
    } catch (err) {
      console.error(err);
      return next(err);
    }
  })
  .post(authenticateMiddleware, async (req, res, next) => {
    try {
      const { title, body } = req.body;
      const user = (req as any).user;

      const result = await NewsService.create(user, title, body);

      return res.send(result).status(201);
    } catch (err) {
      console.log(err);
      return next(err);
    }
  });

NewsController.route('/news/:id')
  .get(async (req, res, next) => {
    try {
      const { id } = req.params;
      const result = await NewsService.getById(id);
      return res.json(result);
    } catch (err) {
      console.log(err);
      if ((err as any).code === '22P02') {
        return next(new ValidationError('Should be uuid format', '.id'));
      }
      return next(err);
    }
  })
  .put(authenticateMiddleware, async (req, res, next) => {
    try {
      const {
        params: { id },
        body
      } = req;

      const { id: userId } = (req as any).user;

      const result = await NewsService.updateById(id, body, userId);

      return res.json(result);
    } catch (err) {
      console.log(err);
      return next(err);
    }
  });

export { NewsController };

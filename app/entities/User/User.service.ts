import { getRepository } from 'typeorm';
import * as jwt from 'jsonwebtoken';
import * as crypto from 'crypto';
import { validate } from 'class-validator';

import { User } from '@app/entities';
import { HttpError, salt, secret, tokenExpiration, ValidationError } from '@app/common';

export class UserService {
  public static findById(id: string) {
    return getRepository(User).findOne(id);
  }

  public static async validateNotExist(email: string) {
    const userInstance = await getRepository(User).findOne({ email });
    return userInstance !== undefined;
  }

  public static generateUserToken(userId: string) {
    return jwt.sign({ userId: userId }, secret, {
      expiresIn: tokenExpiration,
      algorithm: 'HS256',
      jwtid: this.generateHash(userId.toString())
    });
  }

  public static async create(name: string, email: string, password: string) {
    const isExist = await UserService.validateNotExist(email);
    if (isExist) {
      throw new HttpError(422, 'User already exist');
    }

    const userInstance = new User(name, email, password);
    const errors = await validate(userInstance);

    if (errors.length) {
      const firstError = errors[0];
      throw new ValidationError(JSON.stringify(firstError.constraints), `.${firstError.property}`);
    }

    userInstance.password = this.generateHash(password);

    await getRepository(User).save(userInstance);

    return { token: this.generateUserToken(userInstance.id), user: userInstance.formatToResponse() };
  }

  public static async login(email: string, inputPassword: string) {
    const user = await getRepository(User).findOneOrFail({ email, password: this.generateHash(inputPassword) });
    const isValidPassword = user.password === this.generateHash(inputPassword);
    if (!isValidPassword) {
      throw new ValidationError('Incorrect password', '.password');
    }
    return { token: this.generateUserToken(user.id), user: user.formatToResponse() };
  }

  private static generateHash(password: string): string {
    return crypto
      .createHash('sha256')
      .update(salt + password)
      .digest('hex');
  }
}

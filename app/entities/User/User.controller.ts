import * as express from 'express';
import { UserService } from '@app/entities';
import { ValidationError } from '@app/common';

const UserController = express.Router();

UserController.route('/user').post(async (req, res, next) => {
  try {
    const { name, password, email } = req.body;

    const result = await UserService.create(name, email, password);

    return res.json(result);
  } catch (err) {
    console.error(err);
    return next(err);
  }
});

UserController.route('/user/login').post(async (req, res, next) => {
  try {
    const { password, email } = req.body;
    const result = await UserService.login(email, password);

    return res.json(result);
  } catch (err) {
    console.error(err);
    if (err.name === 'EntityNotFound') {
      return next(new ValidationError('User with such email not found', '.email'));
    }
    return next(err);
  }
});

export { UserController };

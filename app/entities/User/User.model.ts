import { Column, Entity, PrimaryGeneratedColumn } from 'typeorm';
import { IsEmail, Length } from 'class-validator';

@Entity()
export class User {
  constructor(name: string, email: string, password: string) {
    this.name = name;
    this.email = email;
    this.password = password;
  }

  @PrimaryGeneratedColumn('uuid')
  public id: string;

  @Column({ nullable: false })
  @Length(2, 20)
  public name: string;

  @Column({ unique: true, nullable: false })
  @IsEmail()
  public email: string;

  @Column({ nullable: false })
  @Length(8, 20)
  public password: string;

  public formatToResponse() {
    const { password, ...rest } = this;
    return rest;
  }
}

import 'should';
import * as request from 'supertest';
import { Express } from 'express';

import { NewsEngineAPI } from '@app/service';
import { TestHelper } from '@app/common';
import { getRepository } from 'typeorm';
import { User } from '@app/entities';

const app = new NewsEngineAPI();
let server: Express | undefined;

describe('User controller test', () => {
  before(async () => {
    await app.start();
    await TestHelper.createFakeDatabase();
    server = app.server;
  });

  after(async () => {
    await TestHelper.removeFakeDatabase();
    await app.stop();
  });

  describe('POST /api/user', () => {
    const body = { name: 'SomeName', email: 'someEmail@gmail.com', password: 'somePassword' };

    it('should create user', async () => {
      const { body: response } = await request(server)
        .post('/api/user')
        .send(body)
        .expect(200);

      validateUserResponse(response, body.name, body.email);
    });

    it('should not create already existed user', async () => {
      await request(server)
        .post('/api/user')
        .send(body)
        .expect(422);
    });

    const correctBody = { name: 'SomeName', email: 'someNewEmail@gmail.com', password: 'somePassword' };

    [[], {}, 12, [{}, {}], false, undefined].forEach(incorrectValue => {
      ['email', 'name', 'password'].forEach(incorrectKey => {
        it(`should return 422 ${incorrectKey}: ${incorrectValue}`, async () => {
          await request(server)
            .post('/api/user')
            .send({ ...correctBody, [incorrectKey]: incorrectValue })
            .expect(422);
        });
      });
    });

    it('should return 422 with incorrect params', async () => {
      await request(server)
        .post('/api/user')
        .send({ ...body, email: 'not-email' })
        .expect(422);
    });
  });

  describe('POST /api/user/login', () => {
    const successLoginBody = { email: 'login@gmail.com', password: 'loginPassword' };

    const userData = { ...successLoginBody, name: 'loginName' };

    before(async () => {
      await request(server)
        .post('/api/user')
        .send(userData)
        .expect(200);
    });

    it('should login successfully', async () => {
      const user = await getRepository(User).findOneOrFail({ email: userData.email });

      const { body: response } = await request(server)
        .post('/api/user/login')
        .send({ email: userData.email, password: userData.password })
        .expect(200);

      validateUserResponse(response, user.name, user.email);
    });

    ['password', 'email'].forEach(key => {
      it(`should not login with incorrect ${key}`, async () => {
        await request(server)
          .post('/api/user/login')
          .send({ ...successLoginBody, [key]: 'incorrectValue' })
          .expect(422);
      });
    });
  });
});

function validateUserResponse(response: any, name: string, email: string) {
  response.token.should.be.type('string');
  response.user.name.should.be.eql(name);
  response.user.email.should.be.eql(email);
}

import { MigrationInterface, QueryRunner } from 'typeorm';
import { TestHelper } from '@app/common';

export class FakeDatabase1567948834187 implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<any> {
    await TestHelper.createFakeDatabase();
  }

  public async down(queryRunner: QueryRunner): Promise<any> {
    await TestHelper.removeFakeDatabase();
  }
}

export class HttpError extends Error {
  public status: number;
  public error: any;

  constructor(statusCode: number, error: any) {
    super();
    this.status = statusCode;
    this.error = error;
  }
}

export function createValidationError(dataPath: string, message: string) {
  return {
    error: {
      message: 'Validation error',
      details: [
        {
          dataPath,
          message
        }
      ]
    }
  };
}

export class ValidationError extends HttpError {
  constructor(message: string, path: string = '') {
    super(422, createValidationError(path, message));
  }
}

import { NextFunction, Request, Response } from 'express';
import * as passport from 'passport';
import { HttpError } from '@app/common';

export function authenticateMiddleware(req: Request, res: Response, next: NextFunction) {
  passport.authenticate('jwt', (err: any, user: any, info: any) => {
    if (user) {
      req.user = user;
      return next();
    } else {
      return next(new HttpError(401, 'Unauthorized'));
    }
  })(req, res, next);
}

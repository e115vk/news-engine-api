export const secret = process.env.JWT_SECRET!;
export const salt = process.env.SALT!;
export const tokenExpiration = process.env.JWT_EXPIRATION!;
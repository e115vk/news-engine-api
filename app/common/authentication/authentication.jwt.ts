import { ExtractJwt, Strategy as JwtStrategy, StrategyOptions, VerifiedCallback } from 'passport-jwt';
import { secret } from '@app/common/authentication/authentication.const';
import { UserService } from '@app/entities';

const opts: StrategyOptions = {
  jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
  secretOrKey: secret
};

export const jwtStrategy = new JwtStrategy(opts, async (jwtPayload, done: VerifiedCallback) => {
  const user = await UserService.findById(jwtPayload.userId);

  if (user) {
    return done(null, user);
  }

  return done(null, null);
});
